from rest_framework.serializers import ModelSerializer

from .models import (
    User,
    Housing,
    Comment,
    Order,
    OrderItem,
)


class UserSerializer(ModelSerializer):
    """User serializer"""

    class Meta:
        """Meta"""

        model = User
        fields = ('email', 'first_name', 'last_name', 'phone_number', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """Register new user"""
        user = User(
            email=validated_data.get('email')
        )
        user.set_password(validated_data.get('password'))
        user.save()
        return user


class CommentSerializer(ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Comment
        fields = '__all__'


class HousingSerializer(ModelSerializer):
    owner = UserSerializer()

    class Meta:
        model = Housing
        fields = '__all__'


class OrderSerializer(ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'


class OrderItemSerializer(ModelSerializer):
    housing = HousingSerializer()

    class Meta:
        model = OrderItem
        fields = '__all__'
