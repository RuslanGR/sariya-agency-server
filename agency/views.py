from rest_framework.generics import CreateAPIView, ListCreateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authentication import BasicAuthentication

from .serializers import (
    UserSerializer,
    HousingSerializer,
    OrderItemSerializer,
)
from .models import (
    Housing,
    OrderItem,
    Order,
    User,
)


class RegistrationView(CreateAPIView):

    permission_classes = (AllowAny,)
    serializer_class = UserSerializer


class HousingList(ListCreateAPIView):

    authentication_classes = (BasicAuthentication,)
    permission_classes = (AllowAny, )
    serializer_class = HousingSerializer
    queryset = Housing.objects.all()


class OrderAPIView(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, _, pk):
        user = User.objects.get(pk=pk)
        order, _ = Order.objects.get_or_create(user=user)
        items = order.orderitem_set.all()
        serialized_items = OrderItemSerializer(items, many=True)

        return Response(serialized_items.data)

    def post(self, request, pk):
        # Send housing id to add it to cart
        data = request.data['data']
        order, _ = Order.objects.get_or_create(user__pk=pk)
        housing = Housing.objects.get(pk=data['id'])
        order_item, _ = OrderItem.objects.get_or_create(housing=housing, order=order)
        if 'delete' in data:
            order_item.delete()

        return Response({})
