# Generated by Django 2.2 on 2019-05-01 21:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('agency', '0004_order_orderitem'),
    ]

    operations = [
        migrations.AddField(
            model_name='housing',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10),
        ),
    ]
