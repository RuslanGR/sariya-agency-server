from django.contrib import admin

from .models import User, Housing, Comment, OrderItem, Order


admin.site.register(User)
admin.site.register(Housing)
admin.site.register(Comment)
admin.site.register(OrderItem)
admin.site.register(Order)
