from django.urls import path

from .views import HousingList, OrderAPIView


urlpatterns = (
    path('housing/', HousingList.as_view()),
    path('order/<int:pk>/', OrderAPIView.as_view()),
)
